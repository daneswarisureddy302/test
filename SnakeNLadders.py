import pygame
from random import randint
import time

clock = pygame.time.Clock()
pygame.init()
width = 1366
height = 768

graphics_display = pygame.display.set_mode((width, height), pygame.FULLSCREEN)
pygame.display.set_caption("Snakes N Ladders")
pygame.display.update()

menubg = pygame.image.load("menu3.png")


pink = (255, 105, 180)
black = (10, 10, 10)
white = (250, 250, 250)
red = (200, 0, 0)
bright_red = (240, 0, 0)
green = (0, 200, 0)
bright_green = (0, 230, 0)
#blue = (0, 0, 200)
#grey = (50, 50, 50)
yellow = (150, 150, 0)



def message_display(text, x, y, fs):
    largeText = pygame.font.Font('freesansbold.ttf', fs)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = (x, y)
    graphics_display.blit(TextSurf, TextRect)


def text_objects(text, font):
    textSurface = font.render(text, True, white)
    return textSurface, textSurface.get_rect()


def button(text, xmouse, ymouse, x, y, w, h, i, a, fs, b):
    if x + w > xmouse > x and y + h > ymouse > y:
        pygame.draw.rect(graphics_display, a, [x - 2.5, y - 2.5, w + 5, h + 5])
        if pygame.mouse.get_pressed() == (1, 0, 0):
            if b == 1:
                options()
            elif b == 5:
                return 5
            elif b == 0:
                Quit()
            elif b == "s" or b == 2 or b == 3 or b == 4:
                return b
            elif b == 7:
                options()
            else:
                return True
    else:
        pygame.draw.rect(graphics_display, i, [x, y, w, h])
    message_display(text, (x + w + x) / 2, (y + h + y) / 2, fs)


def main():
    menu = True
    while menu:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                Quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    Quit()

        # 
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()

        graphics_display.blit(menubg, (0, 0))
        button("Play", mouse[0], mouse[1], (width / 2 - 100),
               height / 2, 200, 100, green, bright_green, 60, 1)
        button("Quit", mouse[0], mouse[1], (width / 2-100),
               (height / 2) + 200, 200, 100, red, bright_red, 60, 0)
        pygame.display.update()


def Quit():
    pygame.quit()
    quit()


main()
